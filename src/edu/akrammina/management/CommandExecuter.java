package edu.akrammina.management;

import java.util.*;

import edu.akrammina.management.classes.*;
import edu.akrammina.management.exceptions.*;
import edu.akrammina.management.utilities.*;

/**
 * Class containing the command executer for the database. This takes in the
 * command and parameters from the main program {@link Bibliography} and
 * executes that command on the database
 *
 * @author Mina Akram
 */
public class CommandExecuter {
    /**
     * the database that will contain all the data and will perform the data
     * processing
     */
    private Database database;

    /**
     * Constructor for the command executer
     */
    public CommandExecuter() {
        this.database = new Database();
    }

    /**
     * Method to print a list of articles
     *
     * @param articles
     *            the list of articles to be printed
     * @return string presentation of the list of articles
     */
    private String articlePrinting(final HashSet<Article> articles) {
        String output = "";
        for (final Article a : articles) {
            output += a + "\n";
        }
        return output.trim();
    }

    /**
     * Method to return a list of authors as a string
     *
     * @param authors
     *            the list of authors to be printed
     * @return the string representation of the authors list
     */
    private String authorPrinting(final HashSet<Author> authors) {
        String output = "";
        for (Author a : authors) {
            output += a.toString() + "\n";
        }
        return output.trim();
    }

    /**
     * Method to split string of authors into a list of authors
     *
     * @param authorsString
     *            string containing the needed authors
     * @return a list of the authors from the string
     * @throws InvalidInputException
     *             if one of the author names cannot be used for an author, or
     *             the list is empty
     */
    private List<Author> authorSplitting(final String authorsString) throws InvalidInputException {
        final List<String> names = Arrays.asList(this.customSplit(authorsString, ";"));
        if (names.isEmpty()) {
            throw new InvalidInputException("there is an empty list in the input");
        }
        final List<Author> authors = new LinkedList<>();
        for (final String name : names) {
            if (name.split(" ").length != 2) {
                throw new InvalidInputException(name + " is not a valid author name.");
            }
            authors.add(new Author(this.customSplit(name, " ")[0], this.customSplit(name, " ")[1]));
        }
        return authors;
    }

    /**
     * Method to split the commands that require splitting using firstly colons,
     * then commas
     *
     * @param command
     *            the command to be executed
     * @param parameters
     *            parameters required for the execution of the command
     * @return the string "Ok"
     * @throws InvalidInputException
     *             if there are insufficient parameters or incorrect input for
     *             the command
     * @throws InvalidEntityException
     *             if the entity required does not exist
     */
    private String cmdWithCommaSplitting(final Commands command, final String parameters)
            throws InvalidInputException, InvalidEntityException {
        final String[] parametersArray = this.customSplit(parameters, ":");
        if (parametersArray.length < 2) {
            throw new InvalidInputException("insufficient number of parameters");
        }
        final String[] params = {"", parametersArray[parametersArray.length - 1] };
        for (int i = 0; i < (parametersArray.length - 2); i++) {
            params[0] += parametersArray[i] + ":";
        }
        params[0] += parametersArray[parametersArray.length - 2];
        String id = "";
        switch (command) {
            case ADD_ARTICLE_TO_JOURNAL:
            case ADD_ARTICLE_TO_SERIES:
                id = params[1];
                break;
            case ADD_KEYWORDS_TO_JOURNAL:
            case ADD_KEYWORDS_TO_PUBLICATION:
            case ADD_KEYWORDS_TO_SERIES:
            case ADD_KEYWORDS_TO_CONFERENCE:
                id = params[0];
                break;
            default:
                throw new InvalidInputException("the command that is input is incorrect");
        }
        final String[] splitID = this.customSplit(id, ",");
        if (splitID.length != command.getParameterCount()) {
            throw new InvalidInputException("insufficient number of parameters");
        }
        switch (command) {
            case ADD_ARTICLE_TO_JOURNAL:
                this.database.addArticleToJournal(params[0], new Article(splitID[0], splitID[1], splitID[2]));
                return "Ok";
            case ADD_ARTICLE_TO_SERIES:
                this.database.addArticleToSeries(params[0], new Article(splitID[0], splitID[1], splitID[2]));
                return "Ok";
            case ADD_KEYWORDS_TO_JOURNAL:
                this.database.addKeywordsToJournal(splitID[0], this.listSplitting(params[1]));
                return "Ok";
            case ADD_KEYWORDS_TO_PUBLICATION:
                this.database.addKeywordsToArticle(splitID[0], this.listSplitting(params[1]));
                return "Ok";
            case ADD_KEYWORDS_TO_SERIES:
                this.database.addKeywordsToSeries(splitID[0], this.listSplitting(params[1]));
                return "Ok";
            case ADD_KEYWORDS_TO_CONFERENCE:
                this.database.addKeywordsToConference(splitID[0], splitID[1], this.listSplitting(params[1]));
                return "Ok";
            default:
                throw new InvalidInputException("the command that is input is incorrect");
        }
    }

    /**
     * Method to split the commands that require splitting using commas
     *
     * @param command
     *            the command to be executed
     * @param parameters
     *            parameters required for the execution of the command
     * @return either "Ok", or the appropriate output from the {@link Database}
     * @throws InvalidInputException
     *             if there are insufficient parameters or incorrect input for
     *             the command
     * @throws InvalidEntityException
     *             if the entity required does not exist
     */
    private String cmdWithSimpleSplitting(final Commands command, final String parameters)
            throws InvalidInputException, InvalidEntityException {
        final String[] params = this.customSplit(parameters, ",");
        if (params.length != command.getParameterCount()) {
            throw new InvalidInputException(
                    command.getName() + " expects " + command.getParameterCount() + " parameters");
        }
        switch (command) {
            case ADD_AUTHOR:
                this.database.addAuthor(new Author(params[0], params[1]));
                return "Ok";
            case ADD_CONFERENCE:
                this.database.addConference(params[0], new Conference(params[1], params[2]));
                return "Ok";
            case ADD_JOURNAL:
                this.database.addJournal(new Journal(params[0], params[1]));
                return "Ok";
            case CITES:
                this.database.cites(params[0], params[1]);
                return "Ok";
            case IN_PROCEEDINGS:
                return this.articlePrinting(this.database.inProceedings(params[0], params[1]));
            case SIMILARITY:
                return this.database.similarity(params[0], params[1]);
            case WRITTEN_BY:
                this.database.addWrittenBy(params[0], new LinkedHashSet<>(this.authorSplitting(params[1])));
                return "Ok";
            default:
                throw new InvalidInputException("the command that is input is incorrect");
        }
    }

    /**
     * Method to split the commands that require splitting using spaces
     *
     * @param command
     *            the command to be executed
     * @param parameters
     *            parameters required for the execution of the command
     * @return the output from the {@link Database}
     * @throws InvalidInputException
     *             if there are insufficient parameters or incorrect input for
     *             the command
     * @throws InvalidEntityException
     *             if the entity required does not exist
     */
    private String cmdWithSpaceSplitting(final Commands command, final String parameters)
            throws InvalidInputException, InvalidEntityException {
        final String[] params = this.customSplit(parameters, " ");
        if (command.getParameterCount() != params.length) {
            throw new InvalidInputException(
                    command.getName() + " expects " + command.getParameterCount() + " parameters");
        }
        switch (command) {
            case H_INDEX:
                return "" + this.database.hIndex(new Author(params[0], params[1]));
            case COAOUTHORS_OF:
                return this.authorPrinting(this.database.coauthorsOf(new Author(params[0], params[1])));
            case FOREIGN_CITATIONS_OF:
                return this.articlePrinting(this.database.foreignCitation(new Author(params[0], params[1])));
            case JACCARD:
                return FormatUtils.toThreeDecimalPlaceString(CalculationUtils.jaccardCalc(
                        new HashSet<>(this.listSplitting(params[0])), new HashSet<>(this.listSplitting(params[1]))));
            default:
                throw new InvalidInputException("the command that is input is incorrect");
        }
    }

    /**
     * Method to execute the direct print methods
     *
     * @param command
     *            the command to be executed
     * @param parameters
     *            parameters required for the execution of the command
     * @return the requested print format
     * @throws InvalidInputException
     *             if there are insufficient parameters or incorrect input for
     *             the command
     * @throws InvalidEntityException
     *             if the entity required does not exist
     */
    private String commandDirectPrints(final Commands command, final String parameters)
            throws InvalidInputException, InvalidEntityException {
        final String[] parametersArray = this.customSplit(parameters, ":");
        if (parametersArray.length < 2) {
            throw new InvalidInputException("insufficient number of parameters");
        }
        final String[] params = {parametersArray[0], "" };
        for (int i = 1; i < (parametersArray.length - 1); i++) {
            params[1] += parametersArray[i] + ":";
        }
        params[1] += parametersArray[parametersArray.length - 1];
        final String[] values = this.customSplit(params[1], ",");
        if ((values.length > command.getParameterCount()) || (values.length < (command.getParameterCount() - 2))) {
            throw new InvalidInputException("insufficient number of parameters");

        }
        final LinkedHashSet<Author> authors = new LinkedHashSet<>();
        switch (command) {
            case DIRECT_PRINT_CONFERENCE:
                for (int i = 0; i < (values.length - 4); i++) {
                    if (this.customSplit(values[i], " ").length != 2) {
                        throw new InvalidInputException(values[i] + " is not a valid author name!");
                    }
                    authors.add(new Author(this.customSplit(values[i], " ")[0], this.customSplit(values[i], " ")[1]));
                }
                break;
            case DIRECT_PRINT_JOURNAL:
                for (int i = 0; i < (values.length - 3); i++) {
                    if (this.customSplit(values[i], " ").length != 2) {
                        throw new InvalidInputException(values[i] + " is not a valid author name!");
                    }
                    authors.add(new Author(this.customSplit(values[i], " ")[0], this.customSplit(values[i], " ")[1]));
                }
                break;
            default:
                break;
        }
        switch (params[0]) {
            case "ieee": {
                switch (command) {
                    case DIRECT_PRINT_CONFERENCE:
                        return FormatUtils.directPrintIEEEConference(authors, values[values.length - 4],
                                values[values.length - 3], values[values.length - 2], values[values.length - 1]);
                    case DIRECT_PRINT_JOURNAL:
                        return FormatUtils.directPrintIEEEJournal(authors, values[values.length - 3],
                                values[values.length - 2], values[values.length - 1]);
                    default:
                        throw new InvalidInputException("the only available styles are ieee and chicago");
                }
            }
            case "chicago": {
                switch (command) {
                    case DIRECT_PRINT_CONFERENCE:
                        return FormatUtils.directPrintChicagoConference(authors, values[values.length - 4],
                                values[values.length - 3], values[values.length - 2], values[values.length - 1]);
                    case DIRECT_PRINT_JOURNAL:
                        return FormatUtils.directPrintChicagoJournal(authors, values[values.length - 3],
                                values[values.length - 2], values[values.length - 1]);
                    default:
                        throw new InvalidInputException("the only available styles are ieee and chicago");
                }
            }
            default:
                throw new InvalidInputException("the only available styles are ieee and chicago");
        }
    }

    /**
     * Method to execute the commands that require no splitting of the
     * parameters
     *
     * @param command
     *            the command to be executed
     * @param parameters
     *            parameters required for the execution of the command
     * @return the string "Ok" or the appropriate output of the {@link Database}
     * @throws InvalidInputException
     *             if there are insufficient parameters or incorrect input for
     *             the command
     * @throws InvalidEntityException
     *             if the entity required does not exist
     */
    private String commandsWithNoSplittingNeeded(final Commands command, final String parameters)
            throws InvalidEntityException, InvalidInputException {
        switch (command) {
            case ADD_CONFERENCE_SERIES:
                if (parameters.length() == 0) {
                    throw new InvalidInputException(command.getName() + " needs one parameter");
                }
                this.database.addSeries(new Series(parameters));
                return "Ok";
            case ALL_PUBLICATIONS:
                if (parameters.length() != 0) {
                    throw new InvalidInputException(command.getName() + " needs no parameters after it");
                }
                return this.articlePrinting(this.database.allArticles());
            case LIST_INVALID_PUBLICATIONS:
                if (parameters.length() != 0) {
                    throw new InvalidInputException(command.getName() + " needs no parameters after it");
                }
                return this.articlePrinting(this.database.listInvalidArticles());
            case PUBLICATIONS_BY:
                if (parameters.length() == 0) {
                    throw new InvalidInputException(command.getName() + " needs one parameter");
                }
                return this.articlePrinting(this.database.publicationsBy(this.authorSplitting(parameters)));
            case FIND_KEYWORDS:
                if (parameters.length() == 0) {
                    throw new InvalidInputException(command.getName() + " needs one parameter");
                }
                return this.articlePrinting(this.database.findKeywords(this.listSplitting(parameters)));
            case DIRECT_H_INDEX:
                if (parameters.length() == 0) {
                    throw new InvalidInputException(command.getName() + " needs one parameter");
                }
                return "" + CalculationUtils.directHIndexCalc(this.numberSplitting(parameters));
            default:
                throw new InvalidInputException("the command that is input is incorrect");
        }
    }

    /**
     * Method to split the given string using the given splitter, while removing
     * any empty strings from the split
     *
     * @param parameters
     *            the string of the parameters to be split
     * @param splitter
     *            the string to split according to
     * @return an array containing the elements after the split is performed
     */
    private String[] customSplit(final String parameters, final String splitter) {
        final LinkedList<String> part = new LinkedList<>(Arrays.asList(parameters.split(splitter)));
        part.removeIf(a -> a.trim().equals(""));
        String[] array = new String[part.size()];
        array = part.toArray(array);
        return array;
    }

    /**
     * Method to execute any one of the available commands in the database
     *
     * @param command
     *            command to be executed from {@link Commands}
     * @param parameters
     *            the string of parameters required for the command's execution
     * @return the appropriate string result of the command
     * @throws InvalidInputException
     *             if the input can't be used in the database, due to wrong
     *             format
     * @throws InvalidEntityException
     *             if a non-existent entity is called
     */
    public String execute(final Commands command, final String parameters)
            throws InvalidInputException, InvalidEntityException {
        switch (command) {
            case ADD_CONFERENCE_SERIES:
            case ALL_PUBLICATIONS:
            case LIST_INVALID_PUBLICATIONS:
            case PUBLICATIONS_BY:
            case FIND_KEYWORDS:
            case DIRECT_H_INDEX:
                return this.commandsWithNoSplittingNeeded(command, parameters);
            case ADD_AUTHOR:
            case ADD_CONFERENCE:
            case ADD_JOURNAL:
            case CITES:
            case IN_PROCEEDINGS:
            case SIMILARITY:
            case WRITTEN_BY:
                return this.cmdWithSimpleSplitting(command, parameters);
            case H_INDEX:
            case COAOUTHORS_OF:
            case FOREIGN_CITATIONS_OF:
            case JACCARD:
                return this.cmdWithSpaceSplitting(command, parameters);
            case ADD_ARTICLE_TO_JOURNAL:
            case ADD_ARTICLE_TO_SERIES:
            case ADD_KEYWORDS_TO_JOURNAL:
            case ADD_KEYWORDS_TO_PUBLICATION:
            case ADD_KEYWORDS_TO_SERIES:
            case ADD_KEYWORDS_TO_CONFERENCE:
                return this.cmdWithCommaSplitting(command, parameters);
            case DIRECT_PRINT_CONFERENCE:
            case DIRECT_PRINT_JOURNAL:
                return this.commandDirectPrints(command, parameters);
            case PRINT_BIBLIOGRAPHY: {
                final String[] params = this.customSplit(parameters, ":");
                if (params.length != 2) {
                    throw new InvalidInputException("insufficient number of parameters");
                }
                switch (params[0]) {
                    case "ieee":
                        return this.database.printBibliographyIEEE(this.listSplitting(params[1]));
                    case "chicago":
                        return this.database.printBibliographyChicago(this.listSplitting(params[1]));
                    default:
                        throw new InvalidInputException("the only available styles are ieee and chicago");
                }
            }
            default:
                throw new InvalidInputException("the command that is input is incorrect");
        }
    }

    /**
     * Method to split any given list of parameters, returning it as a list
     *
     * @param parameters
     *            the list of parameters to be split
     * @return list containing all the parameters from the given string
     * @throws InvalidInputException
     *             if the list is empty
     */
    private List<String> listSplitting(final String parameters) throws InvalidInputException {
        final List<String> list = new LinkedList<>(Arrays.asList(this.customSplit(parameters, ";")));
        if (list.isEmpty()) {
            throw new InvalidInputException("there is an empty list in the input");
        }
        return list;
    }

    /**
     * Method to split a list of numbers from a string
     *
     * @param parameters
     *            the string containing the numbers to be split
     * @return list in {@link Integer} value from the list
     * @throws InvalidInputException
     *             if the list contains any values that are not numbers
     */
    private List<Integer> numberSplitting(final String parameters) throws InvalidInputException {
        final List<String> list = new LinkedList<>(Arrays.asList(this.customSplit(parameters, ";")));
        if (list.isEmpty()) {
            throw new InvalidInputException("there is an empty list in the input");
        }
        final List<Integer> numbers = new LinkedList<>();
        try {
            for (final String s : list) {
                numbers.add(Integer.parseInt(s));
            }
        } catch (final NumberFormatException e) {
            throw new InvalidInputException("only numbers are expected in this command");
        }
        return numbers;
    }
}
