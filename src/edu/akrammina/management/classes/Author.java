package edu.akrammina.management.classes;

import static edu.akrammina.management.utilities.ValidationUtils.isValidAuthorNameFormat;

import java.util.HashSet;

import edu.akrammina.management.exceptions.InvalidInputException;

/**
 * Class used to present the authors in the database.
 *
 * @author Mina Akram
 */
public class Author implements Comparable<Author> {
    /**
     * The first name of the author.
     */
    private final String firstName;
    /**
     * The last name of the author.
     */
    private final String lastName;
    /**
     * List of the publication IDs written by the author.
     */
    private final HashSet<String> publicationIDs;

    /**
     * Constructor of an author using their first and last name.
     *
     * @param firstName
     *            first name of the author.
     * @param lastName
     *            last name of the author.
     * @throws InvalidInputException
     *             if the names input cannot be used to create an author.
     */
    public Author(final String firstName, final String lastName) throws InvalidInputException {
        if (!isValidAuthorNameFormat(firstName) || !isValidAuthorNameFormat(lastName)) {
            throw new InvalidInputException("the author name input is of a wrong format.");
        }
        this.firstName = firstName;
        this.lastName = lastName;
        this.publicationIDs = new HashSet<>();
    }

    /**
     * Method to add a publication to the author
     *
     * @param pubID
     *            ID of the article to be added to the author
     */
    public void addPublication(final String pubID) {
        this.publicationIDs.add(pubID);
    }

    @Override
    public int compareTo(final Author o) {
        if (this.equals(o)) {
            return 0;
        }
        if (o.getLastName().equals(this.getLastName())) {
            return this.getFirstName().compareTo(o.getFirstName());
        } else {
            return this.getLastName().compareTo(o.getLastName());
        }
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final Author other = (Author) obj;
        if (this.firstName == null) {
            if (other.firstName != null) {
                return false;
            }
        } else if (!this.firstName.equals(other.firstName)) {
            return false;
        }
        if (this.lastName == null) {
            if (other.lastName != null) {
                return false;
            }
        } else if (!this.lastName.equals(other.lastName)) {
            return false;
        }
        return true;
    }

    /**
     * Method to get the first name of the author.
     *
     * @return the first name of the author.
     */
    public String getFirstName() {
        return this.firstName;
    }

    /**
     * Method to get the last name of the author.
     *
     * @return the last name of the author.
     */
    public String getLastName() {
        return this.lastName;
    }

    /**
     * Method to get the IDs of the publications written by the author.
     *
     * @return list of all the publication IDs written by the author.
     */
    public HashSet<String> getPubIDs() {
        return this.publicationIDs;
    }

    @Override
    public String toString() {
        return this.firstName + " " + this.lastName;
    }
}
