package edu.akrammina.management.classes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;

import edu.akrammina.management.exceptions.InvalidInputException;
import edu.akrammina.management.utilities.ValidationUtils;

/**
 * A class for the article entity
 *
 * @author Mina Akram
 *
 */
public class Article extends Entity implements Comparable<Article> {
    /**
     * Linked Hash Set of the authors that wrote the article
     */
    private final LinkedHashSet<Author> authors;
    /**
     * Hash Set of the IDs of the articles that cite the current article
     */
    private final HashSet<String> citedBy;
    /**
     * Hash Set of the articles that this article cites
     */
    private final HashSet<String> cites;
    /**
     * The ID of this article
     */
    private final String pubID;
    /**
     * The title of the current article
     */
    private final String title;
    /**
     * The year this article was written in
     */
    private final int year;

    /**
     * Constructor of the article, which checks that the data is of the correct
     * format for an article
     *
     * @param pubID
     *            ID of the article
     * @param year
     *            year the article was written in
     * @param title
     *            title of the article
     * @throws InvalidInputException
     *             thrown when any of the given data is of the wrong format for
     *             an article
     */
    public Article(final String pubID, final String year, final String title) throws InvalidInputException {
        super();
        if (!ValidationUtils.isValidPublicationIDFormat(pubID)) {
            throw new InvalidInputException("this publication ID is of the wrong format: " + pubID);
        }
        if (!ValidationUtils.isValidYearFormat(year)) {
            throw new InvalidInputException("this year format is invalid: " + year);
        }
        if (!ValidationUtils.isValidTitle(title)) {
            throw new InvalidInputException("this is not a valid title: " + title);
        }
        this.pubID = pubID;
        this.title = title;
        try {
            this.year = Integer.parseInt(year);
        } catch (final NumberFormatException e) {
            throw new InvalidInputException("this year format is invalid " + year);
        }
        this.authors = new LinkedHashSet<>();
        this.citedBy = new HashSet<>();
        this.cites = new HashSet<>();
    }

    /**
     * Method to add an author to the article
     *
     * @param author
     *            author to be added
     */
    public void addAuthor(final Author author) {
        this.authors.add(author);
    }

    /**
     * Method to add a citation to the article
     *
     * @param pubID
     *            the ID of the article to be cited
     */
    public void addCitation(final String pubID) {
        this.cites.add(pubID);
    }

    /**
     * Method to add the ID of an article that cited this article
     *
     * @param pubID
     *            ID of the article that cited this article
     */
    public void addCitedBy(final String pubID) {
        this.citedBy.add(pubID);
    }

    /**
     * Method to check if this article cites the article with the given ID
     *
     * @param pubID
     *            ID of the article to be checked
     * @return True if this article is cited, false otherwise
     */
    public boolean cites(final String pubID) {
        return this.cites.contains(pubID);
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(final Article o) {
        final ArrayList<Author> thisAuthors = new ArrayList<>(this.authors);
        final ArrayList<Author> otherAuthors = new ArrayList<>(o.getAuthors());
        final int minimum = Math.min(thisAuthors.size(), otherAuthors.size());
        for (int i = 0; i < minimum; i++) {
            if (thisAuthors.get(i).compareTo(otherAuthors.get(i)) != 0) {
                return (thisAuthors.get(i).compareTo(otherAuthors.get(i)));
            }
        }
        if (otherAuthors.size() > thisAuthors.size()) {
            return -1;
        } else if (otherAuthors.size() < thisAuthors.size()) {
            return 1;
        }
        if (this.title.compareTo(o.getTitle()) != 0) {
            return this.title.compareTo(o.getTitle());
        }
        if ((this.year - o.year) != 0) {
            return this.year - o.year;
        }
        return this.pubID.compareTo(o.pubID);
    }

    /**
     * Method to check if this article contains all the given keywords
     *
     * @param keywords
     *            keywords to be checked
     * @return True if this article contains all the given keywords, false
     *         otherwise
     */
    public boolean containsKeywords(final Collection<? extends String> keywords) {
        return this.getKeywords().containsAll(keywords);
    }

    /**
     * Method to get all the authors that wrote this article
     *
     * @return the authors that wrote the article
     */
    public LinkedHashSet<Author> getAuthors() {
        return this.authors;
    }

    /**
     * Method to the IDs of the articles that cite this article
     *
     * @return the IDs that cite this article
     */
    public HashSet<String> getCitedBy() {
        return this.citedBy;
    }

    /**
     * Method to get the IDs of the articles that this article cites
     *
     * @return the IDs of the articles this article cite
     */
    public HashSet<String> getCites() {
        return this.cites;
    }

    /**
     * Method to get the ID of this article
     *
     * @return the ID of this article
     */
    public String getPubID() {
        return this.pubID;
    }

    /**
     * Method to get the title of this article
     *
     * @return the title of this article
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * Method to get the year this article was written in
     *
     * @return the year this article was written in
     */
    public int getYear() {
        return this.year;
    }

    /**
     * Method to check if the given article cites this article
     *
     * @param pubID
     *            ID of the article to be checked
     * @return True if the given article cites this article, false otherwise
     */
    public boolean isCitedBy(final String pubID) {
        return this.citedBy.contains(pubID);
    }

    /**
     * Method to check if the given author wrote this article
     *
     * @param author
     *            author to be checked
     * @return True if the author wrote this article, false otherwise
     */
    public boolean isPublicationBy(final Author author) {
        return this.authors.contains(author);
    }

    /**
     * Method to check if this article is valid
     *
     * @return True if this article is valid, false otherwise
     */
    public boolean isValid() {
        return !this.authors.isEmpty();
    }

    /*
     * (non-Javadoc)
     * @see akram.mina.literature_management.classes.Entity#toString()
     */
    @Override
    public String toString() {
        return this.pubID;
    }

}
