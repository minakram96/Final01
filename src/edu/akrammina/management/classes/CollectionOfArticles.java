package edu.akrammina.management.classes;

import java.util.Collection;
import java.util.HashSet;

import edu.akrammina.management.exceptions.InvalidInputException;

/**
 * Abstract class used as an upper class for all entities that contain a
 * collection of articles.
 *
 * @author Mina Akram
 *
 */
public abstract class CollectionOfArticles extends Entity {
    /**
     * Method to get all the publication IDs in the collection.
     *
     * @return List of all the articles in the collection.
     */
    public abstract HashSet<String> allPubIDs();

    /**
     * Method to check if the collection contains an article with the given ID.
     *
     * @param pubID
     *            ID of the article to be found.
     * @return True if the collection contains the article, false otherwise.
     */
    public abstract boolean containsArticle(final String pubID);

    /**
     * Method to get all the articles from the collection containing the given
     * keywords.
     *
     * @param keywords
     *            list of keywords to be checked for.
     * @return all the keywords in the collection that contain the given
     *         keywords.
     * @throws InvalidInputException
     *             if one of the given keywords is invalid.
     */
    public abstract HashSet<Article> findKeywords(Collection<? extends String> keywords) throws InvalidInputException;

    /**
     * Method to get all the publications in the given collection.
     *
     * @return List of all the articles in the given collection.
     */
    public abstract HashSet<Article> getAllArticles();

    /**
     * Returns the Article with the given ID
     *
     * @param pubID
     *            the ID of article to be found
     * @return The article with the given ID
     */
    public abstract Article getArticle(final String pubID);

    /**
     * Method to list all the invalid articles in the given collection.
     *
     * @return list of the invalid keywords in the collection.
     */
    public abstract HashSet<Article> invalidArticles();
}
