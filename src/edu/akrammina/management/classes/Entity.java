package edu.akrammina.management.classes;

import static edu.akrammina.management.utilities.ValidationUtils.isValidKeywordFormat;

import java.util.Collection;
import java.util.HashSet;

import edu.akrammina.management.exceptions.InvalidInputException;

/**
 * Abstract class of all the entities on the database
 *
 * @author Mina Akram
 *
 */
public abstract class Entity {
    /**
     * The keywords of the every entity
     */
    private final HashSet<String> keywords;

    /**
     * Constructor for every entity
     */
    public Entity() {
        this.keywords = new HashSet<>();
    }

    /**
     * Method to add keywords to an entity, after checking that they are valid
     *
     * @param keywords
     *            collection of keywords to be added
     * @throws InvalidInputException
     *             thrown when one of the keywords are not valid
     */
    public void addKeywords(final Collection<? extends String> keywords) throws InvalidInputException {
        this.validateKeywords(keywords);
        this.keywords.addAll(keywords);
    }

    /**
     * Method to return all the keywords that have been added to the entity
     *
     * @return the keywords added to the entity
     */
    public HashSet<String> getKeywords() {
        return this.keywords;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public abstract String toString();

    /**
     * Method to check that the given keywords are valid
     *
     * @param keywords
     *            collection of keywords to be added
     * @throws InvalidInputException
     *             thrown when one of the words are not valid
     */
    public void validateKeywords(final Collection<? extends String> keywords) throws InvalidInputException {
        for (final String a : keywords) {
            if (!isValidKeywordFormat(a)) {
                throw new InvalidInputException("the keyword " + a + " is of a wrong format.");
            }
        }
    }

}
