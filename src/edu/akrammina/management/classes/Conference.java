package edu.akrammina.management.classes;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

import edu.akrammina.management.exceptions.InvalidEntityException;
import edu.akrammina.management.exceptions.InvalidInputException;
import edu.akrammina.management.utilities.ValidationUtils;

public class Conference extends Entity {
    /**
     * Hashmap of the {@link Article}s presented at the conference.
     */
    private final HashMap<String, Article> articles;
    /**
     * Location where the conference took place.
     */
    private final String location;
    /**
     * Year in which the conference took place.
     */
    private final int year;

    /**
     * Constructor for the conference.
     *
     * @param year
     *            year in which the conference took place.
     * @param location
     *            location in which the conference took place.
     * @throws InvalidInputException
     *             if the input is of the wrong format.
     */
    public Conference(final String year, final String location) throws InvalidInputException {
        super();
        if (!ValidationUtils.isValidLocationFormat(location)) {
            throw new InvalidInputException("this is not a valid format for the location " + location);
        }
        if (!ValidationUtils.isValidYearFormat(year)) {
            throw new InvalidInputException("this year format is invalid " + year);
        }
        try {
            this.year = Integer.parseInt(year);
        } catch (final NumberFormatException e) {
            throw new InvalidInputException("this year format is invalid " + year);
        }
        this.location = location;
        this.articles = new HashMap<>();
    }

    /**
     * Method to add an article into the conference.
     *
     * @param article
     *            {@link Article} to be added to the conference
     * @throws InvalidEntityException
     *             if there is another {@link Article} with the same ID in the
     *             conference already.
     * @throws InvalidInputException
     *             if the keywords in the conference are not valid and therefore
     *             cannot be added to the article.
     */
    public void addArticle(final Article article) throws InvalidEntityException, InvalidInputException {
        if (!this.articles.containsKey(article.getPubID())) {
            article.addKeywords(this.getKeywords());
            this.articles.put(article.getPubID(), article);
        } else {
            throw new InvalidEntityException(
                    "an article arleady exists in the choosen conference with the ID " + article.getPubID());
        }

    }

    @Override
    public void addKeywords(final Collection<? extends String> keywords) throws InvalidInputException {
        super.addKeywords(keywords);
        for (final Article a : this.articles.values()) {
            a.addKeywords(keywords);
        }
    }

    /**
     * Method to get all the publication IDs in the conference.
     *
     * @return list of all the publication IDs in the conference.
     */
    public HashSet<String> allPubIDs() {
        return new HashSet<>(this.articles.keySet());
    }

    /**
     * Method to check if the conference contains an {@link Article} with the
     * given ID.
     *
     * @param pubID
     *            ID of the article to check for.
     * @return True if the conference contains the {@link Article}, false
     *         otherwise.
     */
    public boolean containsArticle(final String pubID) {
        return this.articles.containsKey(pubID);
    }

    /**
     * Method to get all the {@link Article}s in the conference with the given
     * keywords.
     *
     * @param keywords
     *            list of keywords to look for.
     * @return list of {@link Article}s with the given keywords.
     */
    public HashSet<Article> findKeywords(final Collection<? extends String> keywords) {
        final HashSet<Article> articles = new HashSet<>(this.articles.values());
        articles.removeIf(a -> !a.containsKeywords(keywords));
        return articles;
    }

    /**
     * Method to get all the {@link Article}s in the conference.
     *
     * @return set of all the articles in the conference.
     */
    public HashSet<Article> getAllArticles() {
        return new HashSet<>(this.articles.values());
    }

    /**
     * Method to get the {@link Article} with the given ID.
     *
     * @param pubID
     *            Id of the {@link Article} to look for.
     * @return the {@link Article} with the given ID.
     */
    public Article getArticle(final String pubID) {
        if (this.containsArticle(pubID)) {
            return this.articles.get(pubID);
        }
        return null;
    }

    /**
     * Method to get the location of the conference.
     *
     * @return location of the conference.
     */
    public String getLocation() {
        return this.location;
    }

    /**
     * Method to get the year the conference took place.
     *
     * @return the year the conference took place.
     */
    public int getYear() {
        return this.year;
    }

    /**
     * Method to get all the invalid {@link Article}s in the conference
     *
     * @return set of all the invalid {@link Article}s in the conference.
     */
    public HashSet<Article> invalidArticles() {
        final HashSet<Article> articles = new HashSet<>(this.articles.values());
        articles.removeIf(a -> a.isValid());
        return articles;
    }

    @Override
    public String toString() {
        return null;
    }
}
