package edu.akrammina.management.classes;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

import edu.akrammina.management.exceptions.InvalidEntityException;
import edu.akrammina.management.exceptions.InvalidInputException;
import edu.akrammina.management.utilities.ValidationUtils;

/**
 * Class for the Series to be used in the {@link Database}. this extends
 * {@link CollectionOfArticles}.
 *
 * @author Mina Akram
 *
 */
public class Series extends CollectionOfArticles {
    /**
     * Hashmap of all the {@link Conference}s in the series.
     */
    private final HashMap<Integer, Conference> conferences;
    /**
     * Name of the series.
     */
    private final String seriesName;

    /**
     * Constructor for the series.
     *
     * @param seriesName
     *            name of the series.
     * @throws InvalidInputException
     *             if the name of the series is of the wrong format.
     */
    public Series(final String seriesName) throws InvalidInputException {
        super();
        if (!ValidationUtils.isValidSeriesName(seriesName)) {
            throw new InvalidInputException("this is not a valid format for a series " + seriesName);
        }
        this.seriesName = seriesName;
        this.conferences = new HashMap<>();
    }

    /**
     * Method to add an {@link Article} to the series.
     *
     * @param article
     *            {@link Article} to be added.
     * @throws InvalidEntityException
     *             if the article already exists in the conference, or there was
     *             no conference in the year the article was written.
     * @throws InvalidInputException
     *             if the keywords of the conference are invalid, and therefore
     *             cannot be added to the new article.
     */
    public void addArticle(final Article article) throws InvalidEntityException, InvalidInputException {
        if (this.containsConference(article.getYear())) {
            this.conferences.get(article.getYear()).addArticle(article);
        } else {
            throw new InvalidEntityException("there is no conference in the series" + this.getSeriesName()
                    + " in the year " + article.getYear());
        }
    }

    /**
     * Method to add a {@link Conference} to the series
     *
     * @param conference
     *            {@link Conference} to be added.
     * @throws InvalidEntityException
     *             if there is already a {@link Conference} in the given year
     * @throws InvalidInputException
     *             if the keywords from this series cannot be passed down to the
     *             {@link Conference}.
     */
    public void addConference(final Conference conference) throws InvalidEntityException, InvalidInputException {
        if (!this.conferences.containsKey(conference.getYear())) {
            conference.addKeywords(this.getKeywords());
            this.conferences.put(conference.getYear(), conference);
        } else {
            throw new InvalidEntityException("a conference already exists in the year " + conference.getYear());
        }
    }

    @Override
    public void addKeywords(final Collection<? extends String> keywords) throws InvalidInputException {
        super.addKeywords(keywords);
        for (final Conference conf : this.conferences.values()) {
            conf.addKeywords(keywords);
        }
    }

    @Override
    public HashSet<String> allPubIDs() {
        final HashSet<String> pubIDs = new HashSet<>();
        this.conferences.values().forEach(a -> pubIDs.addAll(a.allPubIDs()));
        return pubIDs;
    }

    @Override
    public boolean containsArticle(final String pubID) {
        for (final Conference c : this.conferences.values()) {
            if (c.containsArticle(pubID)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method to check if there is a {@link Conference} in this series in the
     * given year.
     *
     * @param year
     *            year to check for {@link Conference}s.
     * @return True if there was a {@link Conference} in the given year, false
     *         otherwise.
     */
    public boolean containsConference(final Integer year) {
        return this.conferences.containsKey(year);
    }

    @Override
    public HashSet<Article> findKeywords(final Collection<? extends String> keywords) throws InvalidInputException {
        this.validateKeywords(keywords);
        final HashSet<Article> articles = new HashSet<>();
        this.conferences.values().forEach(a -> articles.addAll(a.findKeywords(keywords)));
        return articles;
    }

    @Override
    public HashSet<Article> getAllArticles() {
        final HashSet<Article> articles = new HashSet<>();
        this.conferences.values().forEach(a -> articles.addAll(a.getAllArticles()));
        return articles;
    }

    @Override
    public Article getArticle(final String pubID) {
        if (this.containsArticle(pubID)) {
            for (final Conference c : this.conferences.values()) {
                if (c.containsArticle(pubID)) {
                    return c.getArticle(pubID);
                }
            }
        }
        return null;
    }

    /**
     * Method to get a {@link Conference} from the given year.
     *
     * @param year
     *            year to get the conference of.
     * @return the {@link Conference} in the chosen year.
     */
    public Conference getConference(final Integer year) {
        if (this.containsConference(year)) {
            return this.conferences.get(year);
        }
        return null;
    }

    /**
     * Method to get all the {@link Conference}s in the series.
     *
     * @return set of all the {@link Conference}s in the series.
     */
    public HashSet<Conference> getConferences() {
        return new HashSet<>(this.conferences.values());
    }

    /**
     * Method to get the name of the series.
     *
     * @return the name of the series.
     */
    public String getSeriesName() {
        return this.seriesName;
    }

    @Override
    public HashSet<Article> invalidArticles() {
        final HashSet<Article> articles = new HashSet<>(this.getAllArticles());
        articles.removeIf(a -> a.isValid());
        return articles;
    }

    @Override
    public String toString() {
        return null;
    }

}
