package edu.akrammina.management.classes;

import java.util.*;

import edu.akrammina.management.exceptions.InvalidEntityException;
import edu.akrammina.management.exceptions.InvalidInputException;
import edu.akrammina.management.utilities.ValidationUtils;

/**
 * Class presenting journals for the {@link Database}.
 *
 * @author Mina Akram
 *
 */
public class Journal extends CollectionOfArticles {
    /**
     * Hashmap of all the {@link Article}s in the journal.
     */
    private final HashMap<String, Article> articles;
    /**
     * Name of the journal.
     */
    private final String name;
    /**
     * Name of the publisher of the journal.
     */
    private final String publisher;

    /**
     * Constructor of the journal.
     *
     * @param name
     *            name of the journal.
     * @param publisher
     *            publisher of the journal.
     * @throws InvalidInputException
     *             if the name or publisher are of the wrong format.
     */
    public Journal(final String name, final String publisher) throws InvalidInputException {
        super();
        if (!ValidationUtils.isValidJournalName(name)) {
            throw new InvalidInputException("this is not a valid name for a journal " + name);
        }
        if (!ValidationUtils.isValidPublisherFormat(publisher)) {
            throw new InvalidInputException("this is in an invalid format for a publisher " + publisher);
        }
        this.name = name;
        this.publisher = publisher;
        this.articles = new HashMap<>();
    }

    /**
     * Method to add an article to the journal.
     *
     * @param article
     *            {@link Article} to be added.
     * @throws InvalidEntityException
     *             if there another {@link Article} with the same ID.
     * @throws InvalidInputException
     *             if one of the keywords are invalid.
     */
    public void addArticle(final Article article) throws InvalidEntityException, InvalidInputException {
        if (this.articles.containsKey(article.getPubID())) {
            throw new InvalidEntityException("there exists already an article with the ID" + article.getPubID());
        }
        article.addKeywords(this.getKeywords());
        this.articles.put(article.getPubID(), article);

    }

    @Override
    public void addKeywords(final Collection<? extends String> keywords) throws InvalidInputException {
        super.addKeywords(keywords);
        for (final Article a : this.articles.values()) {
            a.addKeywords(keywords);
        }
    }

    @Override
    public HashSet<String> allPubIDs() {
        return new HashSet<>(this.articles.keySet());
    }

    @Override
    public boolean containsArticle(final String pubID) {
        return this.articles.containsKey(pubID);
    }

    @Override
    public HashSet<Article> findKeywords(final Collection<? extends String> keywords) throws InvalidInputException {
        this.validateKeywords(keywords);
        final HashSet<Article> articles = new HashSet<>(this.articles.values());
        articles.removeIf(a -> !a.containsKeywords(keywords));
        return articles;
    }

    @Override
    public HashSet<Article> getAllArticles() {
        return new HashSet<>(this.articles.values());
    }

    @Override
    public Article getArticle(final String pubID) {
        if (this.containsArticle(pubID)) {
            return this.articles.get(pubID);
        }
        return null;
    }

    /**
     * Method to get the name of the journal.
     *
     * @return the name of the journal.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Method to get the publisher of the journal.
     *
     * @return the name of the publisher of this journal.
     */
    public String getPublisher() {
        return this.publisher;
    }

    @Override
    public HashSet<Article> invalidArticles() {
        final HashSet<Article> articles = new HashSet<>(this.articles.values());
        articles.removeIf(art -> art.isValid());
        return articles;
    }

    @Override
    public String toString() {
        return null;
    }
}
