package edu.akrammina.management;

/**
 * Enum containing all the commands to be used on the database
 *
 * @author Mina Akram
 */
public enum Commands {
    /**
     * Command to add an article to a journal. Requires 3 parameters after the
     * colon, which are the ID of the publication, the year of the publication
     * and the title of the publication.
     */
    ADD_ARTICLE_TO_JOURNAL(
            "add article to journal", 3),
    /**
     * Command to add an article to a series. Requires 3 parameters after the
     * colon, which are the ID of the publication, the year of the publication
     * and the title of the publication.
     */
    ADD_ARTICLE_TO_SERIES(
            "add article to series", 3),
    /**
     * Command to add an author. Requires 2 parameters which are the first and
     * last name of the author.
     */
    ADD_AUTHOR(
            "add author", 2),
    /**
     * Command to add a conference. Requires 3 parameters, which are the series
     * in which the series in which the conference belongs, the year the
     * conference was held and the location of the conference.
     */
    ADD_CONFERENCE(
            "add conference", 3),
    /**
     * Command to add a series. Requires 1 parameter, which is the name of the
     * series.
     */
    ADD_CONFERENCE_SERIES(
            "add conference series", 1),
    /**
     * Command to add a journal. Requires 2 parameters, which are the name of
     * the journal and the publisher of the journal.
     */
    ADD_JOURNAL(
            "add journal", 2),
    /**
     * Command to add keywords to a conference. Requires 2 parameters before the
     * colon, which are the series in which the conference belongs and the year
     * in which the conference was held.
     */
    ADD_KEYWORDS_TO_CONFERENCE(
            "add keywords to conference", 2),
    /**
     * Command to add keywords to a journal. Requires 1 parameter before the
     * colon, which is the name of the journal.
     */
    ADD_KEYWORDS_TO_JOURNAL(
            "add keywords to journal", 1),
    /**
     * Command to add keywords to a publication. Requires 1 parameter before the
     * colon, which is the ID of the article
     */
    ADD_KEYWORDS_TO_PUBLICATION(
            "add keywords to pub", 1),
    /**
     * Command to add keywords to a series. Requires 1 parameter before the
     * colon, which is the name of the series.
     */
    ADD_KEYWORDS_TO_SERIES(
            "add keywords to series", 1),
    /**
     * Command to display all the articles in the database. Requires no
     * parameters.
     */
    ALL_PUBLICATIONS(
            "all publications", 0),
    /**
     * Command to add a citation between two articles. Requires 2 parameters,
     * which are the IDs of the publications.
     */
    CITES(
            "cites", 2),
    /**
     * Command to show the coauthors of an author. Requires 2 paramters, which
     * are the first name and the last name of the author
     */
    COAOUTHORS_OF(
            "coauthors of", 2),
    /**
     * Command to calculate the h-index given the number of times each of their
     * articles were cited. Requires 1 parameter, which is the list of counts.
     */
    DIRECT_H_INDEX(
            "direct h-index", 1),
    /**
     * Command to directly print an article from a conference. Requires a
     * maximum of 7 parameters after the colon, which are 1-3 authors, the title
     * of the article, the name of the series in which the article was
     * presented, the location of the conference where the article was presented
     * and the year in which the article was written.
     */
    DIRECT_PRINT_CONFERENCE(
            "direct print conference", 7),
    /**
     * Command to directly print an article from a journal. Requires a maximum
     * of 6 parameters after the colon, which are 1-3 authors, the title of the
     * article, the name of the journal in which the article was published and
     * the year in which the article was written.
     */
    DIRECT_PRINT_JOURNAL(
            "direct print journal", 6),
    /**
     * Command to find the articles using a list of keywords. Requires 1
     * parameter, which is the list of keywords to look for.
     */
    FIND_KEYWORDS(
            "find keywords", 1),
    /**
     * Command to find the foreign citations of an author. Requires 2
     * parameters, which are the first and last names of the author.
     */
    FOREIGN_CITATIONS_OF(
            "foreign citations of", 2),
    /**
     * Command to find the h-index of an author. Requires 2 parameters which are
     * the first and last names of the author.
     */
    H_INDEX(
            "h-index", 2),
    /**
     * Command to print the articles that are in the given conference. Requires
     * 2 parameters, which are the name of the series to which the conference
     * belongs, and the year of the conference.
     */
    IN_PROCEEDINGS(
            "in proceedings", 2),
    /**
     * Command to calculate the jaccard values between 2 lists of words.
     * Requires 2 parameters, which are the 2 list to be compared.
     */
    JACCARD(
            "jaccard", 2),
    /**
     * Command to list all the invalid articles in the database. Requires no
     * parameters.
     */
    LIST_INVALID_PUBLICATIONS(
            "list invalid publications", 0),
    /**
     * Command to print a list of articles in either IEEE or Chicago format.
     * Requires 1 parameter, which is the list of articles to be printed.
     */
    PRINT_BIBLIOGRAPHY(
            "print bibliography", 1),
    /**
     * Command to print all the publications by a list of authors. Requires 1
     * parameter, which is the list of authors.
     */
    PUBLICATIONS_BY(
            "publications by", 1),
    /**
     * Command to quit the program. Requires no parameters.
     */
    QUIT(
            "quit", 0),
    /**
     * Command to find the similarity between 2 articles, using their keywords.
     * Requires 2 parameters, which are the IDs of the two articles to be
     * compared
     */
    SIMILARITY(
            "similarity", 2),
    /**
     * Command to add a list of authors into an article. Requires 2 parameters,
     * which are the ID of the article and the list of authors that wrote the
     * article.
     */
    WRITTEN_BY(
            "written-by", 2);
    /**
     * The name given to each command
     */
    private final String name;
    /**
     * The number of parameters given to each command
     */
    private final int parameterCount;

    /**
     * Constructor for the commands in the enum
     *
     * @param name
     *            the string representation of the command
     * @param parameterCount
     *            the number of parameters required by the command
     */
    Commands(final String name, final int parameterCount) {
        this.name = name;
        this.parameterCount = parameterCount;
    }

    /**
     * Command to get the command using the appropriate string of the command
     *
     * @param str
     *            string to check if it is a command
     * @return the command from the enum with the given command string
     */
    public static Commands getCommand(final String str) {
        for (final Commands c : Commands.values()) {
            if (str.trim().equals(c.getName())) {
                return c;
            }
        }
        return null;
    }

    /**
     * Method to get the string representation of the command
     *
     * @return name of the command.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Method to get number of parameters required by the command.
     *
     * @return the number of parameters for the given command.
     */
    public int getParameterCount() {
        return this.parameterCount;
    }
}
