package edu.akrammina.management;

import java.util.Collection;
import java.util.HashSet;

import edu.akrammina.management.classes.Article;
import edu.akrammina.management.classes.Journal;
import edu.akrammina.management.exceptions.InvalidEntityException;
import edu.akrammina.management.exceptions.InvalidInputException;

/**
 * Class presenting a list of journals and performs all the appropriate methods.
 * on them
 *
 * @author Mina Akram
 *
 */
public class ListOfJournals extends DatabaseList<Journal> {
    /**
     * Constructor for the list of journals.
     */
    public ListOfJournals() {
        super();
    }

    /**
     * Method to add an article to a journal.
     *
     * @param journalName
     *            name of the journal to add the article to.
     * @param article
     *            Article to be added.
     * @throws InvalidInputException
     *             when there is no journal with the given name.
     * @throws InvalidEntityException
     *             when there exists an article in the journal with the same ID.
     */
    public void addArticleToJournal(final String journalName, final Article article)
            throws InvalidInputException, InvalidEntityException {
        if (!this.getData().containsKey(journalName)) {
            throw new InvalidInputException("there exists no journal with the name " + journalName);
        }
        this.getData().get(journalName).addArticle(article);
    }

    /**
     * Method to a journal to the list.
     *
     * @param journal
     *            journal to be added.
     * @throws InvalidEntityException
     *             if there is another journal with the same name.
     */
    public void addJournal(final Journal journal) throws InvalidEntityException {
        if (this.getData().containsKey(journal.getName())) {
            throw new InvalidEntityException("there exists already a journal with the name " + journal.getName());
        }
        this.getData().put(journal.getName(), journal);
    }

    /**
     * Method to add keywords to a journal.
     *
     * @param journalName
     *            name of the journal to add the articles to.
     * @param keywords
     *            list of keywords to be added.
     * @throws InvalidEntityException
     *             if there is no journal with the given name.
     * @throws InvalidInputException
     *             if one of the keywords is not valid.
     */
    public void addKeywordsToJournal(final String journalName, final Collection<String> keywords)
            throws InvalidEntityException, InvalidInputException {
        if (!this.getData().containsKey(journalName)) {
            throw new InvalidEntityException("there exists no journal with the name: " + journalName);
        }
        this.getData().get(journalName).addKeywords(keywords);
    }

    /**
     * Method to get the journal containing the article with the given ID.
     *
     * @param pubID
     *            ID of the article to search for.
     * @return the journal that contains the given article.
     */
    public Journal getJournalOf(final String pubID) {
        for (final Journal j : this.getData().values()) {
            if (j.containsArticle(pubID)) {
                return j;
            }
        }
        return null;
    }

    /**
     * Method to get all the journals in the list.
     *
     * @return set of all the journals in the list.
     */
    public HashSet<Journal> getJournals() {
        return new HashSet<>(this.getData().values());
    }

}
