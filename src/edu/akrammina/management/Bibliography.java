package edu.akrammina.management;

import edu.akrammina.management.exceptions.InvalidCommandException;
import edu.akrammina.management.exceptions.InvalidEntityException;
import edu.akrammina.management.exceptions.InvalidInputException;
import edu.kit.informatik.Terminal;

/**
 * Main class to read the commands, execute them using the
 * {@link CommandExecuter} and return a result onto the {@link Terminal}.
 *
 * @author Mina Akram
 *
 */
public class Bibliography {
    /**
     * Static variable to check if the quit command has been read yet.
     */
    private static boolean quit = false;

    /**
     * Method to extract a command from the read string from the
     * {@link Commands}.
     *
     * @param input
     *            the string containing the command.
     * @return the command from the {@link Commands} enum.
     * @throws InvalidCommandException
     *             if the given string doesn't contain a valid command.
     */
    public static String commandSeperator(final String input) throws InvalidCommandException {
        final String[] tempArray = input.split(" ");
        String command = "";
        String temp = "";
        for (final String s : tempArray) {
            temp = temp + " " + s;
            if (Commands.getCommand(temp.trim()) != null) {
                command = temp;
            }
        }
        if (command.equals("")) {
            throw new InvalidCommandException("the command input is invalid!");
        }
        return command;
    }

    /**
     * Main method to read from the {@link Terminal} and output the appropriate
     * result
     *
     * @param args
     *            no arguments required.
     */
    public static void main(final String[] args) {
        final CommandExecuter commandExecuter = new CommandExecuter();
        while (!quit) {
            final String commandString = Terminal.readLine();
            try {
                final Commands command = Commands.getCommand(commandSeperator(commandString));
                if (command == Commands.QUIT) {
                    quit = true;
                } else {
                    final String s = commandExecuter.execute(command,
                            commandString.replaceFirst(command.getName(), "").trim());
                    if (!s.equals("")) {
                        Terminal.printLine(s);
                    }
                }
            } catch (final InvalidCommandException e) {
                Terminal.printError(e.getMessage());
            } catch (final InvalidEntityException e) {
                Terminal.printError(e.getMessage());
            } catch (final InvalidInputException e) {
                Terminal.printError(e.getMessage());
            }
        }
        Terminal.printLine("Ok");
    }

}
