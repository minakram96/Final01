package edu.akrammina.management;

import static edu.akrammina.management.utilities.CalculationUtils.*;
import static edu.akrammina.management.utilities.FormatUtils.*;
import static edu.akrammina.management.utilities.SetUtils.union;

import java.util.*;

import edu.akrammina.management.classes.*;
import edu.akrammina.management.exceptions.*;

public class Database {
    /**
     * The list containing all the authors in the database
     */
    private final LinkedList<Author> authors;
    /**
     * A list of all the journals in the database
     */
    private final ListOfJournals journalsList;
    /**
     * a list containing all the series in the database
     */
    private final ListOfSeries seriesList;

    /**
     * Constructor of the Database
     */
    public Database() {
        super();
        this.journalsList = new ListOfJournals();
        this.seriesList = new ListOfSeries();
        this.authors = new LinkedList<>();
    }
    
    /**
     * Method to add an article to a journal
     *
     * @param journalName
     *            name of the journal
     * @param article
     *            article to be added
     * @throws InvalidInputException
     *             thrown if there is another article with the same ID
     * @throws InvalidEntityException
     *             thrown if the journal doesn't exist
     */
    public void addArticleToJournal(final String journalName, final Article article)
            throws InvalidInputException, InvalidEntityException {
        if (this.pubIDExists(article.getPubID())) {
            throw new InvalidInputException("there already exists a publication with the ID: " + article.getPubID());
        }
        this.journalsList.addArticleToJournal(journalName, article);
    }

    /**
     * Method to add an article to a conference series
     *
     * @param seriesName
     *            name of the series to add to
     * @param article
     *            article to be added
     * @throws InvalidInputException
     *             thrown if there is another article with the same ID
     * @throws InvalidEntityException
     *             thrown if the series doesn't exist
     */
    public void addArticleToSeries(final String seriesName, final Article article)
            throws InvalidInputException, InvalidEntityException {
        if (this.pubIDExists(article.getPubID())) {
            throw new InvalidInputException("there already exists a publication with the ID: " + article.getPubID());
        }
        this.seriesList.addArticleToSeries(seriesName, article);
    }

    /**
     * Method to add an author
     *
     * @param author
     *            author to be added
     * @throws InvalidEntityException
     *             thrown if another author with the same name exists
     */
    public void addAuthor(final Author author) throws InvalidEntityException {
        if (this.authors.contains(author)) {
            throw new InvalidEntityException("there exists already an author with the name " + author);
        }
        this.authors.add(author);
    }

    /**
     * Method to add a conference
     *
     * @param series
     *            name of the series to add to
     * @param conference
     *            conference to be added
     * @throws InvalidEntityException
     *             thrown if there is another conference in the same year
     * @throws InvalidInputException
     *             thrown if the series doesn't exist
     */
    public void addConference(final String series, final Conference conference)
            throws InvalidEntityException, InvalidInputException {
        this.seriesList.addConference(series, conference);
    }

    /**
     * Method to add a journal
     *
     * @param journal
     *            journal to be added
     * @throws InvalidEntityException
     *             thrown if another journal with the same name exist
     */
    public void addJournal(final Journal journal) throws InvalidEntityException {
        this.journalsList.addJournal(journal);
    }

    /**
     * Method to add keywords to an article
     *
     * @param pubID
     *            ID of the article
     * @param keywords
     *            list of keywords to be added
     * @throws InvalidInputException
     *             if the keywords are invalid or the article doesn't exist
     */
    public void addKeywordsToArticle(final String pubID, final List<String> keywords) throws InvalidInputException {
        if (!this.pubIDExists(pubID)) {
            throw new InvalidInputException("there is no article with the ID: " + pubID);
        }
        this.getArticle(pubID).addKeywords(keywords);
    }

    /**
     * Method to add keywords to a conference
     *
     * @param seriesName
     *            series name to which the conference belongs
     * @param year
     *            year of the conference
     * @param keywords
     *            list of keywords to add
     * @throws InvalidInputException
     *             if the keywords are not all valid
     * @throws InvalidEntityException
     *             if the conference doesn't exist
     */
    public void addKeywordsToConference(final String seriesName, final String year, final List<String> keywords)
            throws InvalidInputException, InvalidEntityException {
        this.seriesList.addKeywordsToConference(year, seriesName, keywords);
    }

    /**
     * Method to add keywords to a journal
     *
     * @param journalName
     *            name of the journal
     * @param keywords
     *            list of keywords to add
     * @throws InvalidInputException
     *             if the keywords are not all valid
     * @throws InvalidEntityException
     *             if the journal doesn't exist
     */
    public void addKeywordsToJournal(final String journalName, final Collection<String> keywords)
            throws InvalidInputException, InvalidEntityException {
        this.journalsList.addKeywordsToJournal(journalName, keywords);
    }

    /**
     * Method to add keywords to a series
     *
     * @param seriesName
     *            name of the series
     * @param keywords
     *            list of the keywords to add
     * @throws InvalidInputException
     *             if the keywords are not all valid
     * @throws InvalidEntityException
     *             if the series doesn't exist
     */
    public void addKeywordsToSeries(final String seriesName, final Collection<String> keywords)
            throws InvalidInputException, InvalidEntityException {
        this.seriesList.addKeywordsToSeries(seriesName, keywords);
    }

    /**
     * Method to add a series
     *
     * @param series
     *            series to be added
     * @throws InvalidEntityException
     *             if another series already has the name
     */
    public void addSeries(final Series series) throws InvalidEntityException {
        this.seriesList.addSeries(series);
    }

    /**
     * Method to add authors to an article
     *
     * @param pubID
     *            ID of the article to add the authors to
     * @param authors
     *            list of authors to add
     * @throws InvalidEntityException
     *             if the article doesn't exist, or has one of the authors
     */
    public void addWrittenBy(final String pubID, final LinkedHashSet<Author> authors) throws InvalidEntityException {
        if (!this.authors.containsAll(authors)) {
            throw new InvalidEntityException("at least one of the given authors doesn't exist.");
        }
        if (!this.pubIDExists(pubID)) {
            throw new InvalidEntityException("there exists no publication with the ID " + pubID);
        }
        for (final Author a : authors) {
            if (this.findAuthor(a).getPubIDs().contains(pubID)) {
                throw new InvalidEntityException(a + " is already an author of " + pubID);
            }
        }
        authors.forEach(a -> this.findAuthor(a).addPublication(pubID));
        authors.forEach(a -> this.getArticle(pubID).addAuthor(this.findAuthor(a)));
    }

    /**
     * Method to get all the articles in the database
     *
     * @return all the articles in the database
     */
    public HashSet<Article> allArticles() {
        return new HashSet<>(union(this.seriesList.allArticles(), this.journalsList.allArticles()));
    }

    /**
     * Method to add citations of articles
     *
     * @param pubIDCites
     *            ID of the article that cites
     * @param pubIDCited
     *            ID of the article to be cited
     * @throws InvalidInputException
     *             if one of the articles citation is not possible
     */
    public void cites(final String pubIDCites, final String pubIDCited) throws InvalidInputException {
        if (!this.pubIDExists(pubIDCites) || !this.pubIDExists(pubIDCited)) {
            throw new InvalidInputException("at least one of the publications doesn't exist.");
        }
        if (this.getArticle(pubIDCites).getYear() <= this.getArticle(pubIDCited).getYear()) {
            throw new InvalidInputException("publication " + pubIDCites + " cannot cite " + pubIDCited);
        }
        this.getArticle(pubIDCites).addCitation(pubIDCited);
        this.getArticle(pubIDCited).addCitedBy(pubIDCites);
    }

    /**
     * Method to get the coauthors of a given author
     *
     * @param author
     *            author to find coauthors of
     * @return the coauthors of the given author
     * @throws InvalidEntityException
     *             if the author doesn't exist
     */
    public HashSet<Author> coauthorsOf(final Author author) throws InvalidEntityException {
        if (!this.authors.contains(author)) {
            throw new InvalidEntityException("there is no author with the name " + author);
        }
        final HashSet<Author> coauthors = new HashSet<>();
        this.findAuthor(author).getPubIDs().forEach(a -> coauthors.addAll(this.getArticle(a).getAuthors()));
        coauthors.remove(this.findAuthor(author));
        return coauthors;
    }

    private Author findAuthor(final Author author) {
        for (final Author a : this.authors) {
            if (a.equals(author)) {
                return a;
            }
        }
        return null;
    }

    /**
     * Method to get the articles with the given keywords
     *
     * @param keywords
     *            list of keywords to look for
     * @return Articles containing all the given keywords
     * @throws InvalidInputException
     *             if one of the keywords is not valid
     */
    public HashSet<Article> findKeywords(final Collection<String> keywords) throws InvalidInputException {
        return new HashSet<>(union(this.seriesList.findKeywords(keywords), this.journalsList.findKeywords(keywords)));
    }

    /**
     * Method to get the foreign citations of an author
     *
     * @param author
     *            author to look for the foreign citations of
     * @return articles that are foreign citations of the author
     * @throws InvalidEntityException
     *             if the author doesn't exist
     */
    public HashSet<Article> foreignCitation(final Author author) throws InvalidEntityException {
        if (!this.authors.contains(author)) {
            throw new InvalidEntityException("there is no author with the name " + author);
        }
        final HashSet<Author> authors = new HashSet<>(this.coauthorsOf(author));
        authors.add(this.findAuthor(author));
        final HashSet<Article> articles = new HashSet<>();
        // this get the articles that cite an article written by the author
        this.findAuthor(author).getPubIDs().forEach(
                iD -> this.getArticle(iD).getCitedBy().forEach(otherID -> articles.add(this.getArticle(otherID))));
        authors.forEach(auth -> articles.removeIf(art -> art.isPublicationBy(auth)));
        return articles;
    }

    /**
     * Method to get an article using its ID
     *
     * @param pubID
     *            ID of the required article
     * @return the article with the given ID
     */
    private Article getArticle(final String pubID) {
        if (this.journalsList.containsArticle(pubID)) {
            return this.journalsList.getArticle(pubID);
        }
        if (this.seriesList.containsArticle(pubID)) {
            return this.seriesList.getArticle(pubID);
        }
        return null;
    }

    /**
     * Method to get the h-index of an author
     *
     * @param author
     *            author to find the h-index of
     * @return the h-index of the author
     * @throws InvalidInputException
     *             if the author doesn't exist
     */
    public int hIndex(final Author author) throws InvalidInputException {
        if (!this.authors.contains(author)) {
            throw new InvalidInputException("there exists no author with the name " + author.toString());
        }
        final List<Integer> counts = new LinkedList<>();
        this.findAuthor(author).getPubIDs().forEach(a -> counts.add(this.getArticle(a).getCitedBy().size()));
        return directHIndexCalc(counts);
    }

    /**
     * Method to get the proceedings in a conference
     *
     * @param series
     *            name of the series of the conference
     * @param year
     *            year of the conference
     * @return the articles presented at the conference
     * @throws InvalidEntityException
     *             if the conference doesn't exist
     * @throws InvalidInputException
     *             if the data is of the wrong format
     */
    public HashSet<Article> inProceedings(final String series, final String year)
            throws InvalidEntityException, InvalidInputException {
        return this.seriesList.inProceedings(year, series);
    }

    /**
     * Method to list all invalid articles
     *
     * @return the invalid articles in the database
     */
    public HashSet<Article> listInvalidArticles() {
        return new HashSet<>(union(this.seriesList.invalidArticles(), this.journalsList.invalidArticles()));
    }

    /**
     * Method to print the given articles in chicago format
     *
     * @param pubIDs
     *            list of the IDs of the articles to be printed
     * @return the Chicago representation of the articles
     * @throws InvalidInputException
     *             if any of the articles don't exist
     */
    public String printBibliographyChicago(final List<String> pubIDs) throws InvalidInputException {
        this.validatePubIDs(pubIDs);
        String bibliography = "";
        final List<Article> articles = new LinkedList<>();
        pubIDs.forEach(a -> articles.add(this.getArticle(a)));
        Collections.sort(articles);
        for (final Article art : articles) {
            if (this.journalsList.containsArticle(art.getPubID())) {
                bibliography += printChicago(art, this.journalsList.getJournalOf(art.getPubID()))
                        + System.lineSeparator();
            } else {
                bibliography += printChicago(art, this.seriesList.getConferenceOf(art.getPubID()),
                        this.seriesList.getSeriesOf(art.getPubID())) + System.lineSeparator();
            }
        }
        return bibliography.trim();
    }

    /**
     * Method to print the given articles in IEEE format
     *
     * @param pubIDs
     *            list of the IDs of the articles to be printed
     * @return the IEEE representation of the articles
     * @throws InvalidInputException
     *             if any of the articles don't exist
     */
    public String printBibliographyIEEE(final List<String> pubIDs) throws InvalidInputException {
        this.validatePubIDs(pubIDs);
        String bibliography = "";
        int count = 1;
        final List<Article> articles = new LinkedList<>();
        pubIDs.forEach(a -> articles.add(this.getArticle(a)));
        Collections.sort(articles);
        for (final Article art : articles) {
            if (this.journalsList.containsArticle(art.getPubID())) {
                bibliography += printIEEE(count++, art, this.journalsList.getJournalOf(art.getPubID())) + "\n";
            } else {
                bibliography += printIEEE(count++, art, this.seriesList.getConferenceOf(art.getPubID()),
                        this.seriesList.getSeriesOf(art.getPubID())) + "\n";
            }
        }
        return bibliography.trim();
    }

    private boolean pubIDExists(final String pubID) {
        final HashSet<String> pubIDs = new HashSet<>(
                union(this.journalsList.getAllPubIDs(), this.seriesList.getAllPubIDs()));
        return pubIDs.contains(pubID);
    }

    /**
     * Method to print all the articles by a given list of author
     *
     * @param authors
     *            list of authors to print the articles by
     * @return the articles written by all the given authors
     * @throws InvalidInputException
     *             if any of the given authors don't exist
     */
    public HashSet<Article> publicationsBy(final List<Author> authors) throws InvalidInputException {
        if (!this.authors.containsAll(authors)) {
            throw new InvalidInputException("at least of the given authors does not exist.");
        }
        final HashSet<Article> publications = new HashSet<>();
        authors.forEach(
                author -> this.findAuthor(author).getPubIDs().forEach(pub -> publications.add(this.getArticle(pub))));
        return publications;
    }

    /**
     * Method to find the similarity between two given articles
     *
     * @param firstID
     *            ID of the first article
     * @param secondID
     *            ID of the second article
     * @return the similarity between the articles using their keywords
     * @throws InvalidInputException
     *             if one of the articles don't exist
     */
    public String similarity(final String firstID, final String secondID) throws InvalidInputException {
        if (!this.pubIDExists(firstID) || !this.pubIDExists(secondID)) {
            throw new InvalidInputException("at least one of the IDs does not exist");
        }
        return toThreeDecimalPlaceString(
                jaccardCalc(this.getArticle(firstID).getKeywords(), this.getArticle(secondID).getKeywords()));
    }

    /**
     * Method to check that a list of IDs exist and their articles are valid
     *
     * @param pubIDs
     *            list of IDs to be checked
     * @throws InvalidInputException
     *             if one of the IDs is invalid
     */
    private void validatePubIDs(final Collection<String> pubIDs) throws InvalidInputException {
        for (final String iD : pubIDs) {
            if (!this.pubIDExists(iD)) {
                throw new InvalidInputException("there is no publication with the ID " + iD);
            }
            if (!this.getArticle(iD).isValid()) {
                throw new InvalidInputException("the following publication is invalid: " + iD);
            }
        }
    }
}