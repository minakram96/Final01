package edu.akrammina.management;

import static edu.akrammina.management.utilities.ValidationUtils.isValidSeriesName;
import static edu.akrammina.management.utilities.ValidationUtils.isValidYearFormat;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import edu.akrammina.management.classes.Article;
import edu.akrammina.management.classes.Conference;
import edu.akrammina.management.classes.Series;
import edu.akrammina.management.exceptions.InvalidEntityException;
import edu.akrammina.management.exceptions.InvalidInputException;

/**
 * Class for a list of series, with all the appropriate methods to manipulate
 * the list, and this extends {@link DatabaseList}.
 *
 * @author Mina Akram
 */
public class ListOfSeries extends DatabaseList<Series> {
    /**
     * Constructor for the list of series.
     */
    public ListOfSeries() {
        super();
    }

    /**
     * Method to add an {@link Article} to a {@link Series} in the list.
     *
     * @param seriesName
     *            name of the series to be added to.
     * @param article
     *            {@link Article} to be added.
     * @throws InvalidInputException
     *             if there is no conference in the year the article was
     *             written.
     * @throws InvalidEntityException
     *             if there is no {@link Series} with the given name.
     */
    public void addArticleToSeries(final String seriesName, final Article article)
            throws InvalidInputException, InvalidEntityException {
        if (!this.getData().containsKey(seriesName)) {
            throw new InvalidInputException("there exists no series with the name " + seriesName);
        }
        if (!this.getData().get(seriesName).containsConference(article.getYear())) {
            throw new InvalidInputException(
                    "there exists no conference in the given series in the year " + article.getYear());
        }
        this.getData().get(seriesName).addArticle(article);
    }

    /**
     * Method to add a {@link Conference} in a given {@link Series}.
     *
     * @param series
     *            name of the {@link Series} to add the {@link Conference} to.
     * @param conference
     *            the {@link Conference} to be added
     * @throws InvalidEntityException
     *             if the there is no {@link Series} with the given name, or
     *             there is a duplicate of the {@link Conference}.
     * @throws InvalidInputException
     *             if the keywords to be passed to the conference are invalid.
     */
    public void addConference(final String series, final Conference conference)
            throws InvalidEntityException, InvalidInputException {
        if (!this.getData().containsKey(series)) {
            throw new InvalidEntityException("there exists no series with the name: " + series);
        }
        if (this.getData().get(series).containsConference(conference.getYear())) {
            throw new InvalidEntityException("there exists already a conference in the series " + series
                    + " in the year " + conference.getYear());
        }
        this.getData().get(series).addConference(conference);
    }

    /**
     * Method to add keywords to a {@link Conference}.
     *
     * @param year
     *            the year the {@link Conference} took place.
     * @param seriesName
     *            name of the {@link Series} the {@link Conference} belongs to.
     * @param keywords
     *            list of keywords to add to the {@link Conference}.
     * @throws InvalidInputException
     *             if any parts of the input are of the wrong format.
     * @throws InvalidEntityException
     *             if there exists no {@link Conference} in the given
     *             {@link Series} in the given year.
     */
    public void addKeywordsToConference(final String year, final String seriesName, final List<String> keywords)
            throws InvalidInputException, InvalidEntityException {
        if (!isValidYearFormat(year)) {
            throw new InvalidInputException("the year " + year + " is not a valid year.");
        }
        if (!isValidSeriesName(seriesName) || !this.getData().containsKey(seriesName)) {
            throw new InvalidEntityException("there exists no series with the name " + seriesName);
        }
        if (!this.getData().get(seriesName).containsConference(Integer.parseInt(year))) {
            throw new InvalidEntityException(
                    "there is no conference in the series " + seriesName + " in the year " + year);
        }
        this.getData().get(seriesName).getConference(Integer.parseInt(year)).addKeywords(keywords);
    }

    /**
     * Method to add keywords to a {@link Series}.
     *
     * @param seriesName
     *            name of the {@link Series} to add the keywords to.
     * @param keywords
     *            list of the keywords to be added.
     * @throws InvalidEntityException
     *             if there is no {@link Series} with the given name.
     * @throws InvalidInputException
     *             if any of the keywords are of the wrong format (not
     *             consisting of only lowercase single words).
     */
    public void addKeywordsToSeries(final String seriesName, final Collection<String> keywords)
            throws InvalidEntityException, InvalidInputException {
        if (!this.getData().containsKey(seriesName)) {
            throw new InvalidEntityException("there exists no series with the name " + seriesName);
        }
        this.getData().get(seriesName).addKeywords(keywords);
    }

    /**
     * Method to add a {@link Series} into the list.
     *
     * @param series
     *            name of the {@link Series} to be added.
     * @throws InvalidEntityException
     *             if the given name already exists.
     */
    public void addSeries(final Series series) throws InvalidEntityException {
        if (this.getData().containsKey(series.getSeriesName())) {
            throw new InvalidEntityException("there exists already a series with the name " + series.getSeriesName());
        }
        this.getData().put(series.getSeriesName(), series);
    }

    /**
     * Method to get the {@link Conference} in which the {@link Article} with
     * the given ID was presented.
     *
     * @param pubID
     *            ID of the {@link Article} to look for.
     * @return the {@link Conference} in which the {@link Article} was
     *         presented.
     */
    public Conference getConferenceOf(final String pubID) {
        for (final Conference conf : this.getSeriesOf(pubID).getConferences()) {
            if (conf.containsArticle(pubID)) {
                return conf;
            }
        }
        return null;
    }

    /**
     * Method to get all the {@link Series} in the list.
     *
     * @return set of all the {@link Series} in the list.
     */
    public HashSet<Series> getSeries() {
        return new HashSet<>(this.getData().values());
    }

    /**
     * Method to get the {@link Series} in which the {@link Article} with the
     * given ID can be found.
     *
     * @param pubID
     *            ID of the required {@link Article}.
     * @return the {@link Series} containing the {@link Article} with the given
     *         ID.
     */
    public Series getSeriesOf(final String pubID) {
        for (final Series s : this.getData().values()) {
            if (s.containsArticle(pubID)) {
                return s;
            }
        }
        return null;
    }

    /**
     * Method to get all the {@link Article}s presented at a chosen
     * {@link Conference}.
     *
     * @param year
     *            year in which the {@link Conference} took place.
     * @param series
     *            name of the {@link Series} to which the {@link Conference}
     *            belongs.
     * @return the {@link Article}s presented at the chosen {@link Conference}.
     * @throws InvalidEntityException
     *             if the {@link Conference} doesn't exist.
     * @throws InvalidInputException
     *             if the given year is of the wrong format.
     */
    public HashSet<Article> inProceedings(final String year, final String series)
            throws InvalidEntityException, InvalidInputException {
        if (!this.getData().containsKey(series)) {
            throw new InvalidEntityException("there exists no series with the name: " + series);
        }
        if (!isValidYearFormat(year)) {
            throw new InvalidInputException("the format for the year " + year + " is incorrect");
        }
        if (!this.getData().get(series).containsConference(Integer.parseInt(year))) {
            throw new InvalidEntityException(
                    "there exists no conference is the series " + series + " in the year " + year);
        }
        return this.getData().get(series).getConference(Integer.parseInt(year)).getAllArticles();
    }
}
