package edu.akrammina.management.exceptions;

public class InvalidInputException extends Exception {
    /**
     * The serial number for the exception
     */
    private static final long serialVersionUID = -6798011581619715650L;

    /**
     * Constructor for the exception using a message
     *
     * @param message
     *            the message in the exception
     */
    public InvalidInputException(final String message) {
        super(message);
    }

}
