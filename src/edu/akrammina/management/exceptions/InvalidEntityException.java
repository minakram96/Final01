package edu.akrammina.management.exceptions;

public class InvalidEntityException extends Exception {

    /**
     * The serial number for the exception
     */
    private static final long serialVersionUID = -5625143105667982377L;

    /**
     * Constructor for the exception using a message
     *
     * @param message
     *            the message in the exception
     */
    public InvalidEntityException(final String message) {
        super(message);
    }
}
