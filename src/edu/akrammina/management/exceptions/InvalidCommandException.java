package edu.akrammina.management.exceptions;

public class InvalidCommandException extends Exception {
    /**
     * The serial number for the exception
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructor for the exception using a message
     *
     * @param message
     *            the message in the exception
     */
    public InvalidCommandException(final String message) {
        super(message);
    }
}
