package edu.akrammina.management;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

import edu.akrammina.management.classes.Article;
import edu.akrammina.management.classes.CollectionOfArticles;
import edu.akrammina.management.exceptions.InvalidInputException;

/**
 * Abstract class which is the parent for the lists to be used in the database.
 *
 * @author Mina Akram
 *
 * @param <T>
 *            this extends {@link CollectionOfArticles} to ensure that it has
 *            the required methods for the functionality of this class.
 */
public abstract class DatabaseList<T extends CollectionOfArticles> {
    /**
     * Hashmap of the journals or series to be used in the list.
     */
    private final HashMap<String, T> data;

    /**
     * Constructor for the class.
     */
    public DatabaseList() {
        this.data = new HashMap<>();
    }

    /**
     * Method to get all the articles in the {@link CollectionOfArticles}.
     *
     * @return all the {@link Article}s in the {@link CollectionOfArticles}.
     */
    public HashSet<Article> allArticles() {
        final HashSet<Article> articles = new HashSet<>();
        this.data.values().forEach(a -> articles.addAll(a.getAllArticles()));
        return articles;
    }

    /**
     * Method to check if the list contains a {@link CollectionOfArticles} that
     * contains the given {@link Article}.
     *
     * @param pubID
     *            ID of the required {@link Article}.
     * @return True if the {@link Article} is contained in the list, false
     *         otherwise.
     */
    public boolean containsArticle(final String pubID) {
        for (final T s : this.getData().values()) {
            if (s.containsArticle(pubID)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method to get the {@link Article}s that have the given keywords.
     *
     * @param keywords
     *            list of keywords to search for.
     * @return list of all the articles containing the given keywords.
     * @throws InvalidInputException
     *             if one of the keywords is not valid.
     */
    public HashSet<Article> findKeywords(final Collection<? extends String> keywords) throws InvalidInputException {
        final HashSet<Article> articles = new HashSet<>();
        for (final T da : this.data.values()) {
            articles.addAll(da.findKeywords(keywords));
        }
        return articles;
    }

    /**
     * Method to get all the publication IDs in the list of
     * {@link CollectionOfArticles}.
     * 
     * @return list of all the publication IDs.
     */
    public HashSet<String> getAllPubIDs() {
        final HashSet<String> pubIDs = new HashSet<>();
        this.data.values().forEach(a -> pubIDs.addAll(a.allPubIDs()));
        return pubIDs;
    }

    /**
     * Method to get an {@link Article} from the {@link CollectionOfArticles}.
     * 
     * @param pubID
     *            ID of the requested {@link Article}.
     * @return the requested {@link Article}.
     */
    public Article getArticle(final String pubID) {
        if (this.containsArticle(pubID)) {
            for (final T s : this.getData().values()) {
                if (s.containsArticle(pubID)) {
                    return s.getArticle(pubID);
                }
            }
        }
        return null;
    }

    /**
     * Method to get the hashmap of the data
     * 
     * @return the hashmap of the {@link CollectionOfArticles}.
     */
    public HashMap<String, T> getData() {
        return this.data;
    }

    /**
     * Method to get all the invalid {@link Article}s in the list of
     * {@link CollectionOfArticles}.
     * 
     * @return list of the invalid {@link Article}s.
     */
    public HashSet<Article> invalidArticles() {
        final HashSet<Article> invalids = new HashSet<>();
        this.data.values().forEach(a -> invalids.addAll(a.invalidArticles()));
        return invalids;
    }
}
