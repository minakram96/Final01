package edu.akrammina.management.utilities;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Locale;

import edu.akrammina.management.classes.Article;
import edu.akrammina.management.classes.Author;
import edu.akrammina.management.classes.Conference;
import edu.akrammina.management.classes.Journal;
import edu.akrammina.management.classes.Series;
import edu.akrammina.management.exceptions.InvalidInputException;

/**
 * Utility class to print the formats for the database
 *
 * @author Mina Akram
 */
public final class FormatUtils {
    /**
     * Constructor of the Utility class, simply gives an Assertion Error
     */
    private FormatUtils() {
        throw new AssertionError("Utility class cannot be instantiated");
    }
    /**
     * Method to directly print an article in the conference in the Chicago
     * format
     *
     * @param authors
     *            list of the authors that wrote the article
     * @param title
     *            the title of the article
     * @param seriesName
     *            the name of the series that conference is in
     * @param location
     *            the location of the conference
     * @param year
     *            the year of the article and the conference
     * @return the Chicago representation of the article
     * @throws InvalidInputException
     *             thrown when the input data are not valid formats for articles
     *             or conferences
     */
    public static String directPrintChicagoConference(final LinkedHashSet<Author> authors, final String title,
            final String seriesName, final String location, final String year) throws InvalidInputException {
        final Article article = new Article("t1", year, title);
        authors.forEach(a -> article.addAuthor(a));
        final Conference conference = new Conference(year, location);
        final Series series = new Series(seriesName);
        return printChicago(article, conference, series);
    }

    /**
     * Method to directly print an article from a journal using the Chicago
     * format
     *
     * @param authors
     *            list of the authors that wrote the article
     * @param title
     *            title of the article
     * @param journalName
     *            name of the journal that the article is in
     * @param year
     *            the year the article was written
     * @return the Chicago representation of the article
     * @throws InvalidInputException
     *             thrown when the input data are of the wrong format for the
     *             article or journal
     */
    public static String directPrintChicagoJournal(final LinkedHashSet<Author> authors, final String title,
            final String journalName, final String year) throws InvalidInputException {
        final Article article = new Article("t1", year, title);
        authors.forEach(a -> article.addAuthor(a));
        final Journal journal = new Journal(journalName, "NoOne");
        return printChicago(article, journal);
    }

    /**
     * Prints the article in the IEEE format
     *
     * @param authors
     *            list of the authors that wrote the article
     * @param title
     *            title of the article
     * @param seriesName
     *            the name of the series the article belongs to
     * @param location
     *            the location of the conference in which the article was
     *            presented
     * @param year
     *            the year in which the article was written
     * @return IEEE representation of the article
     * @throws InvalidInputException
     *             thrown when the data input cannot be used to represent the
     *             article
     */
    public static String directPrintIEEEConference(final LinkedHashSet<Author> authors, final String title,
            final String seriesName, final String location, final String year) throws InvalidInputException {
        final Article article = new Article("t1", year, title);
        authors.forEach(a -> article.addAuthor(a));
        final Conference conference = new Conference(year, location);
        final Series series = new Series(seriesName);
        return printIEEE(1, article, conference, series);
    }

    /**
     * Prints the IEEE format of an article in a journal
     *
     * @param authors
     *            list of authors that wrote the article
     * @param title
     *            title of the article to be represented
     * @param journalName
     *            the name of the journal in which the article belongs
     * @param year
     *            year in which the article was written
     * @return IEEE representation of the article
     * @throws InvalidInputException
     *             thrown if the input data cannot be used to represent the
     *             article
     */
    public static String directPrintIEEEJournal(final LinkedHashSet<Author> authors, final String title,
            final String journalName, final String year) throws InvalidInputException {
        final Article article = new Article("t1", year, title);
        authors.forEach(a -> article.addAuthor(a));
        final Journal journal = new Journal(journalName, "NoOne");
        return printIEEE(1, article, journal);
    }

    /**
     * Method to print the Chicago format of an article in a conference
     *
     * @param article
     *            the article to be printed
     * @param conference
     *            the conference the article was presented at
     * @param series
     *            the series the article was presented at
     * @return the Chicago representation of the bibliography of the article
     */
    public static String printChicago(final Article article, final Conference conference, final Series series) {
        String chicagoFormat = printChicagoAuthors(article);
        chicagoFormat += ". \"" + article.getTitle() + ".\" Paper presented at " + series.getSeriesName() + ", "
                + conference.getYear() + ", " + conference.getLocation() + ".";
        return chicagoFormat;
    }

    /**
     * Prints the chicago representation of the article and journal given
     *
     * @param article
     *            the article to be printed
     * @param journal
     *            the journal in which the article belongs
     * @return chicago representation of the article
     */
    public static String printChicago(final Article article, final Journal journal) {
        String chicagoFormat = printChicagoAuthors(article);
        chicagoFormat += ". \"" + article.getTitle() + ".\" " + journal.getName() + " (" + article.getYear() + ").";
        return chicagoFormat;
    }

    /**
     * Adds the identifier of the article followed by the list of the authors of
     * the article
     *
     * @param article
     *            the article that is being represented
     * @return identifier of the article in Chicago format, followed by the list
     *         of authors that wrote the article
     */
    private static String printChicagoAuthors(final Article article) {
        final LinkedList<Author> authors = new LinkedList<>(article.getAuthors());
        String authorsString = "(" + authors.get(0).getLastName() + ", " + article.getYear() + ") "
                + authors.get(0).getLastName() + ", " + authors.get(0).getFirstName();
        if (authors.size() > 1) {
            for (int i = 1; i < (authors.size() - 1); i++) {
                authorsString += ", " + authors.get(i).getLastName() + ", " + authors.get(i).getFirstName();
            }
            authorsString += ", and " + authors.get(authors.size() - 1).getLastName() + ", "
                    + authors.get(authors.size() - 1).getFirstName();
        }
        return authorsString;
    }

    /**
     * Method to print the IEEE format of an article in a conference
     *
     * @param count
     *            the identifier of the article
     * @param article
     *            the article to be printed
     * @param conference
     *            the conference the article was presented at
     * @param series
     *            the series the article was presented at
     * @return the IEEE representation of the bibliography of the article
     */
    public static String printIEEE(final int count, final Article article, final Conference conference,
            final Series series) {
        String ieeeFormat = "[" + count + "] " + printIEEEAuthors(article);
        ieeeFormat += ", \"" + article.getTitle() + ",\" in Proceedings of " + series.getSeriesName() + ", "
                + conference.getLocation() + ", " + article.getYear() + ".";
        return ieeeFormat;
    }

    /**
     * Prints the IEEE representation of the given article
     *
     * @param count
     *            identifier of the article
     * @param article
     *            article to be represented
     * @param journal
     *            journal in which the article belongs
     * @return IEEE representation of the article
     */
    public static String printIEEE(final int count, final Article article, final Journal journal) {
        String ieeeFormat = "[" + count + "] " + printIEEEAuthors(article);
        ieeeFormat += ", \"" + article.getTitle() + ",\" " + journal.getName() + ", " + article.getYear() + ".";
        return ieeeFormat;
    }

    /**
     * Adds the list of the authors of the article
     *
     * @param article
     *            the article that is being represented
     * @return IEEE format of the list of authors that wrote the article
     */
    private static String printIEEEAuthors(final Article article) {
        final LinkedList<Author> authors = new LinkedList<>(article.getAuthors());
        String authorsString = authors.get(0).getFirstName().toUpperCase().charAt(0) + ". "
                + authors.get(0).getLastName();
        if (authors.size() > 1) {
            for (int i = 1; i < (authors.size() - 1); i++) {
                authorsString += ", " + authors.get(i).getFirstName().toUpperCase().charAt(0) + ". "
                        + authors.get(i).getLastName();
            }
            authorsString += " and " + authors.get(authors.size() - 1).getFirstName().toUpperCase().charAt(0) + ". "
                    + authors.get(authors.size() - 1).getLastName();
        }
        return authorsString;
    }

    /**
     * Prints the three decimal representation of a number, without rounding
     *
     * @param number
     *            number to be printed
     * @return 3 decimal place representation of the number
     */
    public static String toThreeDecimalPlaceString(final double number) {
        final double num = Math.floor(number * 1000) / 1000.0;
        return String.format(Locale.ROOT, "%.3f", num);
    }
}
