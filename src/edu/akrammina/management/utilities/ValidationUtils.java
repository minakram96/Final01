package edu.akrammina.management.utilities;

import edu.akrammina.management.exceptions.InvalidInputException;

/**
 * Utility class for the validations in the database and entities
 *
 * @author Mina Akram
 */
public final class ValidationUtils {
    /**
     * Constructor for the utility class, simply throws an assertion error
     */
    private ValidationUtils() {
        throw new AssertionError("Utility class cannot be instantiated");
    }

    /**
     * Checks if the string contains a colon
     *
     * @param str
     *            the string to be checked
     * @return True if the string has a colon, false otherwise
     */
    private static boolean hasColon(final String str) {
        return str.matches(".*\\:.*");
    }

    /**
     * Checks if the string contains a comma
     *
     * @param str
     *            the string to be checked
     * @return True if the string has a comma, false otherwise
     */
    private static boolean hasComma(final String str) {
        return str.matches(".*\\,.*");
    }

    /**
     * Checks if the string contains letters
     *
     * @param str
     *            the string to be checked
     * @return True if the string has letters, false otherwise
     */
    private static boolean hasletters(final String str) {
        return str.matches(".*[A-Za-z]+.*");
    }

    /**
     * Checks if the string contains no special characters
     *
     * @param str
     *            the string to be checked
     * @return True if the string has no special characters, false otherwise
     */
    private static boolean hasNoSpecialCharacters(final String str) {
        return str.trim().matches("[A-Za-z0-9\\s]+");
    }

    /**
     * Checks if the string contains numbers
     *
     * @param str
     *            the string to be checked
     * @return True if the string has numbers, false otherwise
     */
    private static boolean hasNumbers(final String str) {
        return str.matches(".*\\d+.*");
    }

    /**
     * Checks if the string contains a semicolon
     *
     * @param str
     *            the string to be checked
     * @return True if the string has a semicolon, false otherwise
     */
    private static boolean hasSemicolon(final String str) {
        return str.matches(".*\\;.*");
    }

    /**
     * Checks if the string contains only lower case characters
     *
     * @param str
     *            the string to be checked
     * @return True if the string has only lower case characters, false
     *         otherwise
     */
    private static boolean isLowerCase(final String str) {
        return str.toLowerCase().equals(str);
    }

    /**
     * Checks if the string is empty
     *
     * @param str
     *            the string to be checked
     * @return True if the string is empty, false otherwise
     */
    private static boolean isNotEmpty(final String str) {
        return !str.trim().equals("");
    }

    /**
     * Checks if the string contains
     *
     * @param str
     *            the string to be checked
     * @return True if the string has only numbers, false otherwise
     */
    private static boolean isOnlyNumbers(final String str) {
        return str.matches("\\d+");
    }

    /**
     * Checks if the string is a single word
     *
     * @param str
     *            the string to be checked
     * @return True if the string is a single word, false otherwise
     */
    private static boolean isSingleWord(final String str) {
        return str.matches("\\S+");
    }

    /**
     * Checks if the given string is a valid author name
     *
     * @param str
     *            the string to be checked
     * @return True if the given string is a valid author name, false otherwise
     */
    public static boolean isValidAuthorNameFormat(final String str) {
        return (isSingleWord(str) && !hasNumbers(str) && !hasComma(str) && !hasSemicolon(str) && !hasColon(str)
                && hasNoSpecialCharacters(str));
    }

    /**
     * Checks if the given string is a valid journal name
     *
     * @param str
     *            the string to be checked
     * @return True if the given string is a valid name for a journal, false
     *         otherwise
     */
    public static boolean isValidJournalName(final String str) {
        return (isNotEmpty(str) && !hasComma(str) && !hasSemicolon(str));
    }

    /**
     * Checks if the given string is a valid keyword
     *
     * @param str
     *            the string to be checked
     * @return True if the given string is a valid keyword, false otherwise
     */
    public static boolean isValidKeywordFormat(final String str) {
        return (isLowerCase(str) && !hasNumbers(str) && isSingleWord(str) && !hasComma(str) && !hasSemicolon(str)
                && hasNoSpecialCharacters(str));
    }

    /**
     * Checks if the given string is a valid location
     *
     * @param str
     *            the string to be checked
     * @return True if the given string is a valid location, false otherwise
     */
    public static boolean isValidLocationFormat(final String str) {
        return (!hasNumbers(str) && hasletters(str) && !hasComma(str) && !hasSemicolon(str));
    }

    /**
     * Checks if the given string is a valid publication ID
     *
     * @param str
     *            the string to be checked
     * @return True if the given string is a valid publication ID, false
     *         otherwise
     */
    public static boolean isValidPublicationIDFormat(final String str) {
        return (isLowerCase(str) && isSingleWord(str) && !hasComma(str) && !hasSemicolon(str)
                && hasNoSpecialCharacters(str));
    }

    /**
     * Checks if the given string is a valid publisher
     *
     * @param str
     *            the string to be checked
     * @return True if the given string is a valid publisher, false otherwise
     */
    public static boolean isValidPublisherFormat(final String str) {
        return (isNotEmpty(str) && !hasComma(str) && !hasSemicolon(str));
    }

    /**
     * Checks if the given string is a valid series name
     *
     * @param str
     *            the string to be checked
     * @return True if the given string is a valid name for a series, false
     *         otherwise
     */
    public static boolean isValidSeriesName(final String str) {
        return (isNotEmpty(str) && !hasComma(str) && !hasSemicolon(str));
    }

    /**
     * Checks if the given string is a valid title for an article
     *
     * @param str
     *            the string to be checked
     * @return True if the given string is a valid title for an article, false
     *         otherwise
     */
    public static boolean isValidTitle(final String str) {
        return isNotEmpty(str) && !hasComma(str) && !hasSemicolon(str);
    }

    /**
     * Checks if the given string is a valid year
     *
     * @param str
     *            the string to be checked
     * @return True if the given string is a valid year, false otherwise
     * @throws InvalidInputException
     *             if the number is too large
     */
    public static boolean isValidYearFormat(final String str) throws InvalidInputException {
        if (isOnlyNumbers(str) && (str.length() == 4)) {
            try {
                return ((Integer.parseInt(str) <= 9999) && (Integer.parseInt(str) >= 1000) && !hasComma(str)
                        && !hasSemicolon(str));
            } catch (final NumberFormatException e) {
                throw new InvalidInputException(str + " is too large of a number");
            }
        }
        return false;
    }
}
