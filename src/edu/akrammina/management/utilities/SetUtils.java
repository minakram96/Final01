package edu.akrammina.management.utilities;

import java.util.HashSet;
import java.util.Set;

/**
 * Utility class for the functions of the sets
 *
 * @author Mina Akram
 */
public final class SetUtils {
    /**
     * Constructor for the utility class, simply throws an Assertion Error
     */
    private SetUtils() {
        throw new AssertionError("Utility class cannot be instantiated");
    }

    /**
     * Finds the intersect of multiple sets
     *
     * @param <T>
     *            generic type for the sets
     * @param sets
     *            the sets to be examined
     * @return set of values in the intersect of all the sets
     */
    @SafeVarargs // used in this program, for a few number of sets
    public static <T> Set<T> intersect(final Set<T>... sets) {
        if (sets[0].isEmpty()) {
            return new HashSet<>();
        }
        final Set<T> intersect = new HashSet<>(sets[0]);
        for (final Set<T> s : sets) {
            intersect.retainAll(s);
        }
        return intersect;
    }

    /**
     * Finds the union of multiple sets
     *
     * @param <T>
     *            the generic type used for the setsS
     * @param sets
     *            the sets to be examined
     * @return set of values in the union of all the sets
     */
    @SafeVarargs // used in this program, for a few number of sets
    public static <T> Set<T> union(final Set<T>... sets) {
        final Set<T> union = new HashSet<>(sets[0]);
        for (final Set<T> s : sets) {
            union.addAll(s);
        }
        return union;
    }
}
