package edu.akrammina.management.utilities;

import static edu.akrammina.management.utilities.SetUtils.intersect;
import static edu.akrammina.management.utilities.SetUtils.union;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import edu.akrammina.management.exceptions.InvalidInputException;

/**
 * Utility class to perform the calculations in the program
 *
 * @author Mina Akram
 */
public final class CalculationUtils {


    /**
     * Constructor for the utility class, that simply returns an Assertion Error
     */
    private CalculationUtils() {
        throw new AssertionError("Utility class cannot be instantiated");
    }
    
    /**
     * Method to calculate the h-index given the number of times the
     * publications of an author were cited
     *
     * @param counts
     *            list of the number of times the publications were cited
     * @return the h-index
     * @throws InvalidInputException
     *             in the case any of the numbers are negative
     */
    public static int directHIndexCalc(final List<Integer> counts) throws InvalidInputException {
        for (final Integer i : counts) {
            if (i < 0) {
                throw new InvalidInputException("only positive numbers are allowed");
            }
        }
        Collections.sort(counts);
        int result = 0;
        for (int i = 0; i < counts.size(); i++) {
            final int smaller = Math.min(counts.get(i), counts.size() - i);
            result = Math.max(result, smaller);
        }
        return result;
    }

    /**
     * Method to calculate the jaccard value of two given HashSets
     *
     * @param firstWords
     *            the set of the first words
     * @param secondWords
     *            the set of the second words
     * @return the jaccard value
     */
    public static Double jaccardCalc(final HashSet<String> firstWords, final HashSet<String> secondWords) {
        return (union(firstWords, secondWords).isEmpty()) ? 1.0
                : (double) intersect(firstWords, secondWords).size() / (double) union(firstWords, secondWords).size();
    }
}
